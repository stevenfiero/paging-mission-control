const fs = require('fs');
const _ = require('lodash');



// Step 1 - Read .txt file. Log original data from file.
function readTxtFile() {
  fs.readFile('inputfile.txt', 'utf-8', function (err, data) {
    if (err) {
      throw 'Error reading file.';
    };
    console.log('Orignial data from txt file: ', '\n', data);
    parseFile(data);
  });
};



// Step 2 - Parse file. Split data and push into new array.
function parseFile(data) {
  dataSplitNL = data.split("\n");
  dataSplitNL.pop();
  let finalParsedData = [];
  for (i = 0; i < dataSplitNL.length; i++) {
    dataSplitBar = dataSplitNL[i].split("|");
    finalParsedData.push(dataSplitBar);
  };
  buildOutputObject(finalParsedData);
};



// Step 3 - Build output object. Create object with key:value pairs as outputKeys:finalParsedData.
function buildOutputObject(finalParsedData) {
  let outputKeys = ['timestamp', 'satelliteId', 'red-high-limit', 'yellow-high-limit', 'yellow-low-limit', 'red-low-limit', 'raw-value', 'component'];
  let outputObj = finalParsedData.map(function (el) {
    let outputObjNested = {};
    for (i = 0; i < el.length; i++) {
      outputObjNested[outputKeys[i]] = el[i];
    };
    return outputObjNested;
  });
  sortViolationConditionTwoThermoHighZero(outputObj);
};



// Step 4 - Sort output object data based on violation condition #2
// Violation Condition: If for the same satellite there are three thermostat readings that exceed
// the red high limit within a five minute interval.
function sortViolationConditionTwoThermoHighZero(outputObj) {
  // Filter by satellite id and thermostat
  let satZeroThermoZeroMatches = _.filter(outputObj, _.matches({'satelliteId': '1000', 'component': 'TSTAT'}));

  // Add key:value pair to identify severity
  let satZeroThermoZero = satZeroThermoZeroMatches.map(function(obj) {
    obj.severity = 'RED HIGH';
    return obj;
  });

  // Filter by satZeroThermoZero that are over red high limit
  let satThermoRedHighLimitZero = [];
  for (i = 0; i < satZeroThermoZero.length; i ++) {
    let redHighLimit = 101;
    if (satZeroThermoZero[i]['raw-value'] > redHighLimit) {
      satThermoRedHighLimitZero.push(satZeroThermoZero[i]);
    };
  };

  // Conditional test if there are no objects that violate the conditional, then log the final output and execute
  // the next function. Else, continue and parse the timestamps and add object to the final output.
  let finalOutputObj = [];
  if (satThermoRedHighLimitZero.length == 0) {
    // console.log('Final output object - #2 Thermo high 0: ', '\n', finalOutputObj);
  } else {
    // Filter satZeroRedHighLimit by five minute interval.
    let satThermoRedHighLimitFiveMinInt = [];
    let satThermoRedHighLimitTimestamps = [];
    for (i = 0; i < satThermoRedHighLimitZero.length; i ++) {
      satThermoRedHighLimitTimestamps.push(satThermoRedHighLimitZero[i]['timestamp']);
    };

    // Parse timestamps.
    // Find min/max timestamps.
    let timestampsMin = _.min(satThermoRedHighLimitTimestamps);
    let timestampsMax = _.max(satThermoRedHighLimitTimestamps);
    // Extract date.
    let dateMin = timestampsMin.slice(0, 7);
    let dateMax = timestampsMax.slice(0, 7);
    // Extract time (hour 24, minute, second, millisecond).
    let timeMin = timestampsMin.slice(9, timestampsMin.length);
    let timeMax = timestampsMax.slice(9, timestampsMax.length);
    // Extract hours.
    let hoursMin = timeMin.slice(0, 2);
    let hoursMax = timeMax.slice(0, 2);
    // Extract minutes.
    let minutesMin = timeMin.slice(4, 6);
    let minutesMax = timeMax.slice(4, 6);

    // Compare dates and hours. If true, compare times.
    let timeDifference;
    if (dateMin == dateMax && hoursMax == hoursMin) {
      timeDifference = parseInt(minutesMax) - parseInt(minutesMin);
    };

    // Return final high thermostat object
    if (timeDifference < 5 && satThermoRedHighLimitTimestamps.length > 2) {
      finalOutputObj.push(satThermoRedHighLimitZero[0]);
    };
  };

  sortViolationConditionTwoThermoHighOne(outputObj, finalOutputObj);
};



// Step 5 - Sort output object data based on violation condition #2
// Violation Condition: If for the same satellite there are three thermostat readings that exceed
// the red high limit within a five minute interval.
function sortViolationConditionTwoThermoHighOne(outputObj, finalOutputObj) {
  // Filter by satellite id and thermostat.
  let satOneThermoOneMatches = _.filter(outputObj, _.matches({'satelliteId': '1001', 'component': 'TSTAT'}));

  // Add key:value pair to identify severity.
  let satOneThermoOne = satOneThermoOneMatches.map(function(obj) {
    obj.severity = 'RED HIGH';
    return obj;
  });

  // Filter by satOneThermoOne that are over red high limit
  let satThermoRedHighLimitOne = [];
  for (i = 0; i < satOneThermoOne.length; i ++) {
    let redHighLimit = 101;
    if (satOneThermoOne[i]['raw-value'] > redHighLimit) {
      satThermoRedHighLimitOne.push(satOneThermoOne[i]);
    } else {
      // console.log('Red high limit: = None');
    };
  };

  // Conditional test if there are no objects that violate the conditional, then log the final output and execute
  // the next function. Else, continue and parse the timestamps and add object to the final output.
  if (satThermoRedHighLimitOne.length == 0) {
    // console.log('Final output object - #2 Thermo high 1: ', '\n', finalOutputObj);
  } else {
    // Filter satZeroRedHighLimit by five minute interval
    let satThermoRedHighLimitFiveMinInt = [];
    let satThermoRedHighLimitTimestamps = [];
    for (i = 0; i < satThermoRedHighLimitOne.length; i ++) {
      satThermoRedHighLimitTimestamps.push(satThermoRedHighLimitOne[i]['timestamp']);
    };

    // Parse timestamps.
    // Find min/max timestamps.
    let timestampsMin = _.min(satThermoRedHighLimitTimestamps);
    let timestampsMax = _.max(satThermoRedHighLimitTimestamps);
    // Extract date.
    let dateMin = timestampsMin.slice(0, 7);
    let dateMax = timestampsMax.slice(0, 7);
    // Extract time (hour 24, minute, second, millisecond).
    let timeMin = timestampsMin.slice(9, timestampsMin.length);
    let timeMax = timestampsMax.slice(9, timestampsMax.length);
    // Extract hours.
    let hoursMin = timeMin.slice(0, 2);
    let hoursMax = timeMax.slice(0, 2);
    // Extract minutes.
    let minutesMin = timeMin.slice(4, 6);
    let minutesMax = timeMax.slice(4, 6);

    // Compare dates and hours. If true, compare times.
    let timeDifference;
    if (dateMin == dateMax && hoursMax == hoursMin) {
      timeDifference = parseInt(minutesMax) - parseInt(minutesMin);
    };

    // Return final high thermostat object.
    if (timeDifference < 5 && satThermoRedHighLimitTimestamps.length > 2) {
      finalOutputObj.push(satThermoRedHighLimitOne[0]);
    };
  };

  sortViolationConditionOneBatteryLowZero(outputObj, finalOutputObj);
};



// Step 6 - Sort output object data based on violation condition #1
// Violation Condition: If for the same satellite there are three battery voltage readings that are under
// the red low limit within a five minute interval.
function sortViolationConditionOneBatteryLowZero(outputObj, finalOutputObj) {
  // Filter by satellite id and battery
  let satZeroBatZeroMatches = _.filter(outputObj, _.matches({'satelliteId': '1000', 'component': 'BATT'}));

  // Add key:value pair to identify severity.
  let satZeroBatZero = satZeroBatZeroMatches.map(function(obj) {
    obj.severity = 'RED LOW';
    return obj;
  });

  // Filter by satZeroBatZero that are under red low limit.
  let satBatRedLowLimitZero = [];
  for (i = 0; i < satZeroBatZero.length; i ++) {
    let redLowLimit = 8;
    if (satZeroBatZero[i]['raw-value'] < redLowLimit) {
      satBatRedLowLimitZero.push(satZeroBatZero[i]);
    };
  };

  // Conditional test if there are no objects that violate the conditional, then log the final output and execute
  // the next function. Else, continue and parse the timestamps and add object to the final output.
  if (satBatRedLowLimitZero.length == 0) {
    // console.log('Final output object - #1 Bat low 0', '\n', finalOutputObj);
  } else {
    // Filter satBatRedLowLimit by five minute interval
    let satBatRedLowLimitFiveMinInt = [];
    let satBatRedLowLimitTimestamps = [];
    for (i = 0; i < satBatRedLowLimitZero.length; i ++) {
      satBatRedLowLimitTimestamps.push(satBatRedLowLimitZero[i]['timestamp']);
    };

    // Parse timestamps.
    // Find min/max timestamps.
    let timestampsMin = _.min(satBatRedLowLimitTimestamps);
    let timestampsMax = _.max(satBatRedLowLimitTimestamps);
    // Extract date.
    let dateMin = timestampsMin.slice(0, 7);
    let dateMax = timestampsMax.slice(0, 7);
    // Extract time (hour 24, minute, second, millisecond).
    let timeMin = timestampsMin.slice(9, timestampsMin.length);
    let timeMax = timestampsMax.slice(9, timestampsMax.length);
    // Extract hours.
    let hoursMin = timeMin.slice(0, 2);
    let hoursMax = timeMax.slice(0, 2);
    // Extract minutes.
    let minutesMin = timeMin.slice(4, 6);
    let minutesMax = timeMax.slice(4, 6);

    // Compare dates and hours. If true, compare times.
    let timeDifference;
    if (dateMin == dateMax && hoursMax == hoursMin) {
      timeDifference = parseInt(minutesMax) - parseInt(minutesMin);
    };

    // Return final low battery object
    if (timeDifference < 5 && satBatRedLowLimitTimestamps.length > 2) {
      finalOutputObj.push(satBatRedLowLimitZero[0]);
    };
  };

  sortViolationConditionOneBatteryLowOne(outputObj, finalOutputObj);
};



// Step 7 - Sort output object data based on violation condition #1
// Violation Condition: If for the same satellite there are three battery voltage readings that are under
// the red low limit within a five minute interval.
function sortViolationConditionOneBatteryLowOne(outputObj, finalOutputObj) {
  // Filter by satellite id and battery
  let satZeroBatOneMatches = _.filter(outputObj, _.matches({'satelliteId': '1001', 'component': 'BATT'}));

  // Add key:value pair to identify severity.
  let satZeroBatOne = satZeroBatOneMatches.map(function(obj) {
    obj.severity = 'RED LOW';
    return obj;
  });

  // Filter by satZeroBatOne that are under red low limit.
  let satBatRedLowLimitOne = [];
  for (i = 0; i < satZeroBatOne.length; i ++) {
    let redLowLimit = 8;
    if (satZeroBatOne[i]['raw-value'] < redLowLimit) {
      satBatRedLowLimitOne.push(satZeroBatOne[i]);
    };
  };

  // Conditional test if there are no objects that violate the conditional, then log the final output and execute
  // the next function. Else, continue and parse the timestamps and add object to the final output.
  if (satBatRedLowLimitOne.length == 0) {
    // console.log('Final output object - #1 Bat low 1: ', '\n', finalOutputObj);
  } else {
    // Filter satBatRedLowLimit by five minute interval
    let satBatRedLowLimitFiveMinInt = [];
    let satBatRedLowLimitTimestamps = [];
    for (i = 0; i < satBatRedLowLimitOne.length; i ++) {
      satBatRedLowLimitTimestamps.push(satBatRedLowLimitOne[i]['timestamp']);
    };

    // Parse timestamps.
    // Find min/max timestamps.
    let timestampsMin = _.min(satBatRedLowLimitTimestamps);
    let timestampsMax = _.max(satBatRedLowLimitTimestamps);
    // Extract date.
    let dateMin = timestampsMin.slice(0, 7);
    let dateMax = timestampsMax.slice(0, 7);
    // Extract time (hour 24, minute, second, millisecond).
    let timeMin = timestampsMin.slice(9, timestampsMin.length);
    let timeMax = timestampsMax.slice(9, timestampsMax.length);
    // Extract hours.
    let hoursMin = timeMin.slice(0, 2);
    let hoursMax = timeMax.slice(0, 2);
    // Extract minutes.
    let minutesMin = timeMin.slice(4, 6);
    let minutesMax = timeMax.slice(4, 6);

    // Compare dates and hours. If true, compare times.
    let timeDifference;
    if (dateMin == dateMax && hoursMax == hoursMin) {
      timeDifference = parseInt(minutesMax) - parseInt(minutesMin);
    };

    // Return final low battery object
    if (timeDifference < 5 && satBatRedLowLimitTimestamps.length > 2) {
      finalOutputObj.push(satBatRedLowLimitOne[0]);
    };
  };

  reorderOutput(finalOutputObj);
};



// Step 8 - Reorder output object format to only include the needed key:value pairs.
// Order - satelliteId, severity, component, timestamp.
function reorderOutput(finalOutputObj) {
  let finalOutputObjFiltered = [];
  for (i = 0; i < finalOutputObj.length; i++) {
    finalOutputObjFiltered.push(_.pick(finalOutputObj[i], ['satelliteId', 'severity', 'component', 'timestamp']));
  };

  jsonOutput(finalOutputObjFiltered)
}



// Step 9 - JSON Stringify the results and log to the console.
function jsonOutput(finalOutputObjFiltered) {
  let jsonObjOutput = JSON.stringify(finalOutputObjFiltered, null, 2);
  console.log('Final JSON output: ', '\n', jsonObjOutput);
}



// Function invocation to read file.
readTxtFile();
